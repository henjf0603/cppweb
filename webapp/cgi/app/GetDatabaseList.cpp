#include <webx/menu.h>
#include <webx/route.h>
#include <dbentity/T_XG_DBETC.h>

class GetDatabaseList : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(GetDatabaseList)

int GetDatabaseList::process()
{
	param_string(id);
	param_string(type);
	param_string(name);
	param_string(enabled);

	webx::CheckFileName(id, 0);
	webx::CheckSystemRight(this);

	string sqlcmd;
	sp<DBConnect> dbconn = webx::GetDBConnect();

	stdx::format(sqlcmd, "SELECT * FROM T_XG_DBETC WHERE ID LIKE '%s%%'", id.c_str());
	
	if (IsNumberString(enabled.c_str()))
	{
		int val = stdx::atoi(enabled.c_str());

		if (val >= 0) stdx::append(sqlcmd, " AND ENABLED=%d", val);
	}

	sqlcmd += " ORDER BY ID ASC";

	json["code"] = webx::PackJson(dbconn->query(sqlcmd), "list", json);
	json["route"] = app->getRouteSwitch();
	out << json;

	return XG_OK;
}