#ifndef XG_HEADER_SYSTEM
#define XG_HEADER_SYSTEM
//////////////////////////////////////////////////////////
#include "utils.h"

#ifndef DAEMON_ENVNAME
#define DAEMON_ENVNAME				"{daemonenvname}"
#endif

#ifdef XG_LINUX

#include <dlfcn.h>
#include <sys/epoll.h>
#include <sys/syscall.h>

typedef void* DLLFILE_T;
void* ThreadFunction(void*);

#define GetCurrentThreadId() syscall(SYS_gettid)

#else

typedef HANDLE DLLFILE_T;
DWORD WINAPI ThreadFunction(void*);

#endif

#define INVALID_DLLFILE		NULL

#ifndef XG_DLLFILE_MAXCNT
#define XG_DLLFILE_MAXCNT	1024
#endif

#ifdef __cplusplus
extern "C" {
#endif
	typedef enum
	{
		eREAD = 1,
		eWRITE = 2,
		eCREATE = 4,
		eHIDDEN = 8,
		eVISIBLE = 16
	} E_OPEN_MODE;

	int GetCpuCount();

	void Flush(HANDLE handle);

	void Close(HANDLE handle);

	int System(const char* cmd);

	const char* GetCurrentUser();

	int CalcCpuUseage(int delay);

	long long Tell(HANDLE handle);

	int RemoveFile(const char* path);
	 
	int GetPathType(const char* path);

	int CreateFolder(const char* path);

	long GetFileLength(const char* path);

	time_t GetFileUpdateTime(const char* path);

	int Rename(const char* src, const char* dest);
	
	int Read(HANDLE handle, void* data, int size);

	HANDLE OpenDisk(int diskno, E_OPEN_MODE mode);

	HANDLE OpenPartition(int ch, E_OPEN_MODE mode);

	int CloneFile(const char* src, const char* dest);

	HANDLE Open(const char* filename, E_OPEN_MODE mode);
	
	int Write(HANDLE handle, const void* data, int size);

	BOOL GetMemoryInfo(long long* total, long long* avail);
	
	long long Seek(HANDLE handle, long long offset, int mode);

	BOOL SetPathAttributes(const char* path, E_OPEN_MODE mode);

	BOOL SetFileUpdateTime(const char* path, const time_t* tm);

	void ColorPrint(E_CONSOLE_COLOR color, const char* fmt, ...);

	BOOL GetCpuTime(long long* usr, long long* idle, long long* kernel);
	
	BOOL ReadSector(HANDLE handle, void* data, long long offset, long long num);

	BOOL WriteSector(HANDLE handle, const void* data, long long offset, long long num);
	
	BOOL GetDiskSpace(const char* path, long long* total, long long* avail, long long* free);

	typedef struct __stWorkItem
	{
		void* data;
		SOCKET sock;
		void(*func)(struct __stWorkItem*);
	} stWorkItem;

	typedef struct __stThreadPool
	{
		int sz;
		int cnt;
		void* threads;
		MutexHandle lock;
	} stThreadPool;

	BOOL IsInvalidThread(THREAD_T thread);

	stThreadPool* ThreadPoolCreate(int maxsz);

	BOOL StartThread(stWorkItem item, const void* attr);

	BOOL ThreadPoolDoWork(stThreadPool* pool, stWorkItem item);

	void DllFileClose(DLLFILE_T handle);

	DLLFILE_T DllFileOpen(const char* path);

	void* DllFileGetAddress(DLLFILE_T handle, const char* name);
#ifdef __cplusplus
}
#endif

//////////////////////////////////////////////////////////
#endif